package at.fh.oo.Test;

import java.util.Scanner;

public class CashBack {
    private float InsertedCash = 0;
    private float CashBack = 0;
    private Automate Automate = new Automate();
    private Scanner myScanner = new Scanner(System.in);

    public void Cashback(){
        float Price = Automate.getPrice();
        System.out.println("You have to pay: " + Price);
        System.out.println("Insert your Cash!");
        float InsertCash = myScanner.nextFloat();

        if(InsertCash < Price){
            System.out.println("Please insert a higher amount of cash");
            InsertCash = myScanner.nextFloat();
        }
        else if(InsertCash > Price){
            System.out.println("Here is your CashBack: " + (InsertCash - Price));
        }
        else if(InsertCash == Price){
            System.out.println("See you!");
        }
    }
}
