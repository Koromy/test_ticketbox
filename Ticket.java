package at.fh.oo.Test;

public class Ticket {

    private int ID;
    private long TimestampOut;
    private long TimestampPayed;

    public Ticket(int ID, long TimestampOut, long TimestampPayed){
        this.ID = ID;
        this.TimestampOut = TimestampOut;
        this.TimestampPayed = TimestampPayed;
    }

    public int getID() {
        return ID;
    }

    public long getTimestampOut() {
        return TimestampOut;
    }

    public long getTimestampPayed() {
        return TimestampPayed;
    }

    public void setTimestampPayed(long timestampPayed) {
        TimestampPayed = timestampPayed;
    }
}
