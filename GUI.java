package at.fh.oo.Test;

import java.util.Scanner;

public class GUI {
    private static Automate Automate = new Automate();
    private static CashBack CashBack = new CashBack();
    private static Scanner myScanner = new Scanner(System.in);

    public static void main(String[] args){

        while(true){
            System.out.println("1: Get Ticket\n2: Show Tickets\n3: Insert Ticket\n4: Pay Ticket");
            int Choice = myScanner.nextInt();

            switch (Choice){
                case 1:
                    Automate.getTicket();
                    break;
                case 2:
                    Automate.showTickets();
                    break;
                case 3:
                    Automate.insertTicket();
                    break;
                case 4:
                    Automate.getPrice();
                    CashBack.Cashback();
                    break;
                default:
                    System.out.println("Error");
                    break;
            }
        }
    }
}
