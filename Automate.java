package at.fh.oo.Test;

import java.sql.Time;
import java.util.*;

public class Automate {

    private java.util.List<Ticket> List = new ArrayList();
    private Date date = new Date();
    private Boolean isInserted = false;
    private Scanner myScanner = new Scanner(System.in);

    public void getTicket(){
        Random Random = new Random();
        int ID = Random.nextInt(1000);
        long TimestampOut = date.getTime();

        Ticket Ticket = new Ticket(ID, TimestampOut, 0);

        System.out.println("Take Ticket from Box");
        List.add(Ticket);
    }

    public void showTickets(){
        for(int i = 0; i < List.size(); i ++){
            System.out.println("Ticket ID: " + List.get(i).getID() + "\nGot at: " + List.get(i).getTimestampOut() + "\nPayed at: " + List.get(i).getTimestampPayed() + "\n");
        }
    }

    public int insertTicket(){
        System.out.println("Insert ID from Ticket");
        int TicketID = myScanner.nextInt();

        isInserted = true;

        System.out.println("Ticket got inserted");
        return TicketID;
    }

    public float getPrice(){
        float Price = 0;

        if(!isInserted){
            System.out.println("Please insert at first your Ticket!");
        }
        else if(isInserted){
            long TimeDifferenceS = 0;
            float PriceMultiplication = 0.5f;
            int ID = insertTicket();
            long TimestampPayed = date.getTime();

            List.get(ID).setTimestampPayed(TimestampPayed);

            TimeDifferenceS = (List.get(ID).getTimestampPayed() - List.get(ID).getTimestampOut())/1000;

           Price = TimeDifferenceS * PriceMultiplication;
        }
        return Price;
        //
    }
}
